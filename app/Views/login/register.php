<h3><?= $title ?></h3>

<form action="/login/registration">
    <div class='col-12'>
        <?= \Config\Services::validation()->listErrors(); ?>
    </div>
    <div class='form-group'>
        <label>Username</label>
        <input class='form-control' name='user' placeholder='Enter username' maxlegth='30'>
    </div>
    <div class='form-group'>
        <label>Firstname</label>
        <input class='form-control' name='fname' placeholder='Enter first name' maxlegth='30'>
    </div>
    <div class='form-group'>
        <label>Firstname</label>
        <input class='form-control' name='lname' placeholder='Enter last name' maxlegth='30'>
    </div>
    <div class='form-group'>
        <label>Password</label>
        <input class='form-control' name='password' type="password" placeholder='Enter password' maxlegth='30'>
    </div>
    <div class='form-group'>
        <label>Password again</label>
        <input class='form-control' name='confirmpassword' type="password" placeholder='Enter password again' maxlegth='30'>
    </div>
    <button class='btn btn-primary'>Submit</button>
</form>