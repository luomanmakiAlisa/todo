<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
    protected $table = 'user';

    protected $allowedFields = ['username', 'password', 'firstname', 'lastname'];

    public function check($username, $password)
    {
        $this->where('username', $username);
        $query = $this->get();
        //print $this->getLastQuery(); // Voi käyttää deduggaamiseen
        $row = $query->getRow(); //hae rivi
        if ($row) { //tarkista onko käyttäjää
            if (password_verify($password, $row->password)) { // tarkista onko salasana oikein
                return $row; // salasana oli oikein
            }
        }
        return null; //tunnus tai salasana väärin
    }
}
