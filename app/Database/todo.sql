drop DATABASE if exists todo;

create database todo;

use todo;

create table user (
    id int primary key auto_increment,
    username VARCHAR(30) not null unique,
    password VARCHAR(255) not null,
    firstname VARCHAR(100),
    lastname VARCHAR(100)
);

create table task (
    id int primary key auto_increment,
    title varchar(255) not null,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    description text,
    user_id int not null,
    index (user_id),
    FOREIGN KEY (user_id) REFERENCES user(id)
    on delete RESTRICT
);


insert into task
values (null, "tehtava1", null, "Olen tehtävä yksi"),
(null, "tehtava2", null, "Olen tehtävä kaksi"),(null, "tehtava3", null, "Olen tehtävä kolme");